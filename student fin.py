from flask import Flask,Response,request
import json
from pymongo import MongoClient
import logging


try:
   conn=MongoClient('localhost',27017,serverSelectionTimeoutMS=1000)
   db=conn.school
   conn.server_info()
except:
   print("ERROR Can't connect to the server")

app=Flask(__name__)

@app.route('/')
def welcome():
   return("The Student Database creation in mongo")


###*********************CREATE******************************
@app.route("/createStd",methods=["PUT"])
def create_student():
   print(request.json)
   try:
      #student=request.json#s{"_id":request.json["_id"],
         #"name":request.json["name"],"class":request.json["class"],"sec":request.json["sec"]}
      dbResponse=db.students.insert_one(request.json)
      print(dbResponse.inserted_id)
      return Response(
         response=json.dumps({"message":"Student entered","id":f"{dbResponse.inserted_id}"}),
         status=200,
         mimetype="application/json"
      )
   except Exception as ex:
      print("*********")
      print(ex)

###*********************DELETE******************************
@app.route('/delStd',methods=['DELETE'])
def delStd():
   try:
      dbResponse=db.students.delete_one({"_id":request.json["_id"]})
      print(dbResponse)
      return Response(
         response=json.dumps({"message":"Student deleted"}),
         status=200,
         mimetype="application/json"
      )
   except Exception as ex:
      print("*********")
      print(ex)
      print("*********")
      return Response(
         response=json.dumps({"message":"Can't delete the data" }),
         status=500,
         mimetype="application/json")

###*********************GET DATA******************************
@app.route('/get1Std/<id>',methods=['GET'])
def get1Std(id):
   try:
      #id={"_id":request.json["_id"]}
      stds=db.students.find_one(id)
      return Response(
         response=json.dumps(stds),
         status=200,
         mimetype="application/json"
      )
   except Exception as ex:
      print("*********")
      print(ex)
      print("*********")
      return Response(
         response=json.dumps({"message":"Can't retrieve the data" }),
         status=500,
         mimetype="application/json")

###*********************GET ALL DATA******************************
@app.route('/getallStd',methods=['GET'])
def getingallstudentsdata():
   try:
      stds=list(db.students.find())
      logging.debug('This is a debug message')
      logging.info('This is an info message')
      logging.warning('This is a warning message')
      logging.error('This is an error message')
      logging.critical('This is a critical message')
      return Response(
         response=json.dumps(stds),
         status=200,
         mimetype="application/json"
      )
   except Exception as ex:
      print("*********")
      print(ex)
      print("*********")
      return Response(
         response=json.dumps({"message":"Can't retrieve the data" }),
         status=500,
         mimetype="application/json")

###*********************UPDATE******************************
@app.route('/updateStd', methods=['PUT'])
def updateStd():
    try:
        dbResponse = db.students.update_one({"_id": request.json["_id"]},
                                         {"$set": {"name": request.json["name"],
                                                   "class": request.json["class"],
                                                   "sec": request.json["sec"]}})
        if dbResponse.modified_count == 1:
            return Response(
                response=json.dumps({"message": "Student details updated"}),
                status=200,
                mimetype="application/json"
            )
        else:
            return Response(
                response=json.dumps({"message": "Nothing to update"}),
                status=200,
                mimetype="application/json"
            )


    except Exception as ex:
        print("********")
        print(ex)
        print("********")
        return Response(
            response=json.dumps({"message": "Sorry cannot update"}),
            status=500,
            mimetype="application/json"
        )

   
if __name__=="__main__":
   app.run(host="0.0.0.0",port=4000)